<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S3-a1: Classes and Objects</title>
</head>
<body>
	<fieldset>
	<h1>Person</h1>
	<!-- <?php var_dump($person); ?> -->

	<p><?= $person->printName(); ?></p>

	<h1>Developer</h1>

	<!-- <?php var_dump($developer); ?> -->

	<p><?= $developer->printName(); ?></p>

	<h1>Developer</h1>

	<!-- <?php var_dump($engineer); ?> -->

	<p><?= $engineer->printName(); ?></p>	
		
	</fieldset>
	
</body>
</html>